# PID_LREAL
PID with LREAL inputs and output


## Description
The standard B&R MTBasicsPID function block calulates with REAL data types. Since a REAL data type has only 6 significant digits, it is not usable when working with positions. Since we needed a PID with a position output, we have decided to write own PID with LREAL data type.

## Usage
The project contains an library PID_LREAL with one function block of the same name. To get this library to your project just use  copy&paste, or the "File->Export Library..." menu in Automation Studio

## Support
This is no B&R official library. It is not supported by B&R.

## Roadmap
TBD

## License
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND.

## Missing functionality
Compared to MTBasicsPID, following functionality is missing:
<ul>
  <li><em>D</em> part is not implemented</li>
  <li><em>Update</em> input not implemented</li>
  <li><em>UpdateDone</em> output not implemented</li>
  <li><em>Busy</em> output not implemented</li>
  <li><em>Error</em> output not implemented</li>
</ul>
