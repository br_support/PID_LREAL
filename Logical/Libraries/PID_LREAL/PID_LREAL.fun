
FUNCTION_BLOCK PID_LREAL
	VAR_INPUT
		Enable : BOOL;
		PIDParameters : PID_LREAL_ParametersType;
		MinOut : LREAL;
		MaxOut : LREAL;
		Update : BOOL;
		Invert : BOOL;
		SetValue : LREAL;
		ActValue : LREAL;
		IntegrationPartPresetValue : LREAL;
		SetIntegrationPart : BOOL;
	END_VAR
	VAR_OUTPUT
		Busy : BOOL;
		Active : BOOL;
		Error : BOOL;
		UpdateDone : BOOL;
		Out : LREAL;
	END_VAR
	VAR
		ControlError : LREAL;
		ProportionalPart : LREAL;
		IntegrationPart : LREAL;
		IntegrationStatus : PID_LREAL_IntegrationEnum;
		Internal : PID_LREAL_InternalType;
	END_VAR
END_FUNCTION_BLOCK
