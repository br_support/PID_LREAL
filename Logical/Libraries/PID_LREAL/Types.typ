
TYPE
	PID_LREAL_InternalType : 	STRUCT 
		CycleTime : REAL;
		SetIntegrationPartOld : BOOL;
		EnableOld : BOOL;
	END_STRUCT;
	PID_LREAL_IntegrationEnum : 
		(
		plr_INTEGRATION_FREE := 0,
		plr_HOLD_INTEGRATION_POSITIVE := +1,
		plr_HOLD_INTEGRATION_NEGATIVE := -1
		);
	PID_LREAL_ParametersType : 	STRUCT 
		Gain : REAL;
		IntegrationTime : REAL;
	END_STRUCT;
END_TYPE
