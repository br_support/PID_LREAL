#include <PID_LREAL.h>

void PID_LREAL(struct PID_LREAL* inst)
{
	struct RTInfo RTInfo_0;
	
	if (inst->Enable)
	{
		inst->Active = 1;
		
		/* determine cycle time */
		if (inst->Internal.EnableOld == 0)
		{
			RTInfo_0.enable = 1;
			RTInfo(&RTInfo_0);	
			inst->Internal.CycleTime = RTInfo_0.cycle_time * 0.000001;
		}
		
		/* 	compute deviation */
		inst->ControlError = inst->SetValue - inst->ActValue;
		
		if (inst->Invert)
			inst->ControlError = -inst->ControlError;

		/*	compute proportional action */
		inst->ProportionalPart = inst->PIDParameters.Gain * inst->ControlError;
			
		/*	compute integral action */
		if (inst->PIDParameters.IntegrationTime > 0)
		{
			if (inst->IntegrationStatus == plr_INTEGRATION_FREE) //anti-windup
			{
				inst->IntegrationPart += inst->ProportionalPart * inst->Internal.CycleTime / inst->PIDParameters.IntegrationTime;
			}
		}
		else
		{
			/*	integral action is not used */
			inst->IntegrationPart = 0;
		}
		
		if (inst->SetIntegrationPart > inst->Internal.SetIntegrationPartOld)
		{
			inst->IntegrationPart = inst->IntegrationPartPresetValue;
		}
		inst->Internal.SetIntegrationPartOld = inst->SetIntegrationPart;

		/*	compute output value */
		inst->Out = inst->ProportionalPart + inst->IntegrationPart;
		
		/* limit output */
		if (inst->Out > inst->MaxOut)
		{
			inst->IntegrationStatus = plr_HOLD_INTEGRATION_POSITIVE;
			inst->Out = inst->MaxOut;
		}
		else if (inst->Out < inst->MinOut)
		{
			inst->IntegrationStatus = plr_HOLD_INTEGRATION_NEGATIVE;
			inst->Out = inst->MinOut;
		}
		else
		{
			inst->IntegrationStatus = plr_INTEGRATION_FREE;
		}
	}
	else
	{
		/*	FUB disabled */
		inst->Active = 0;
		inst->ControlError = 0;
		inst->ProportionalPart = 0;
		inst->IntegrationPart = 0;
		inst->Out = 0;
		inst->IntegrationStatus = plr_INTEGRATION_FREE;
		inst->Internal.SetIntegrationPartOld = 0;
		inst->Internal.CycleTime = 0;
	}
	inst->Internal.EnableOld = inst->Enable;
}
