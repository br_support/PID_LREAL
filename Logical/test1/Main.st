
PROGRAM _INIT
	MTBasicsPWM_0.Enable := TRUE;
	MTBasicsPWM_0.Period := 1;
	MTBasicsPWM_0.DutyCycle := 50;
	
	MTBasicsPID_0.Enable := TRUE;
	
	MTBasicsPID_0.PIDParameters.Gain := 0.0001;
	MTBasicsPID_0.PIDParameters.IntegrationTime := 0.05;
	
	MTBasicsPID_0.MinOut := 0;
	MTBasicsPID_0.MaxOut := 175;
	MTBasicsPID_0.Invert := 1;
	MTBasicsPID_0.SetValue := 4000;	
	MTBasicsPID_0.ActValue := 4031;
	MTBasicsPID_0.IntegrationPartPresetValue := 31.9;
	MTBasicsPID_0.SetIntegrationPart := 1;

END_PROGRAM

PROGRAM _CYCLIC
	MTBasicsPWM_0( );
	
	MTBasicsPID_0.ActValue := 4000 + 1000 * BOOL_TO_INT(MTBasicsPWM_0.Out) - 500;
	MTBasicsPID_0( ); 
	
	PID_LREAL_0.Enable := MTBasicsPID_0.Enable;
	PID_LREAL_0.PIDParameters.Gain := MTBasicsPID_0.PIDParameters.Gain;
	PID_LREAL_0.PIDParameters.IntegrationTime := MTBasicsPID_0.PIDParameters.IntegrationTime;
	PID_LREAL_0.MinOut := MTBasicsPID_0.MinOut;
	PID_LREAL_0.MaxOut := MTBasicsPID_0.MaxOut;
	PID_LREAL_0.Invert := MTBasicsPID_0.Invert;
	PID_LREAL_0.SetValue := MTBasicsPID_0.SetValue;
	PID_LREAL_0.ActValue := MTBasicsPID_0.ActValue;
	PID_LREAL_0.IntegrationPartPresetValue := MTBasicsPID_0.IntegrationPartPresetValue;
	PID_LREAL_0.SetIntegrationPart := MTBasicsPID_0.SetIntegrationPart;
	PID_LREAL_0( );
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

